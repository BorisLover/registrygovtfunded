package com.example.registryGovtFunded.models;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "Adess")
public class Adress {
    //private String cityId;
    @NotBlank(message = "city Name can not be blanc")
    @NotNull(message = "city Name can not be null")
    private String city;
    @NotBlank(message = "state Name can not be blanc")
    @NotNull(message = "state Name can not be null")    
    private String state;
    @NotBlank(message = "pincode can not be blanc")
    @NotNull(message = "pincode can not be null")    
    private String pincode;

    public Adress(){
        //this.cityId="";
        this.city="";
        this.state="";
        this.pincode="";
     }

    public Adress(String uri,String fname,String lname){
        //this.cityId=id;
        this.city=fname;
        this.state=lname;
        this.pincode=uri;
     }

}
