package com.example.registryGovtFunded.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.google.cloud.Timestamp;

import lombok.Data;

@Data
@Entity
@Table(name = "traningCenter")
public class TraningCenter {
    private String documentId;
    @NotBlank(message = "Center Name can not be blanc")
    @NotNull(message = "Center Name can not be null")    
	@Size(min=0, max=39 ,message = "Less than 40 characters")
    private String centerName;
    @Column(length = 12)
    @Size(min=12, max=12 , message = "exactly 12 character alphanumeric")
    private String centerCode;
    private Adress detailedAddress;
    private int studentCapacity;
    private List<String> coursesOffered;
    private Timestamp createdOn;
    @Email(message = "Email is not valide. format (name@namespace.domainName)")
    private String contactEmail;
    @NotBlank(message = "Phone number can not be blanc")
    @NotNull(message = "Phone number can not be null")
    private String contactPhone;

    public TraningCenter(){
        this.documentId="";
        this.centerName="";
        this.centerCode="";
        this.detailedAddress=new Adress();
        this.studentCapacity=-1;
        this.coursesOffered = new ArrayList<String>();
        this.createdOn=Timestamp.now();
        this.contactEmail="";
        this.contactPhone="";
    }

    public TraningCenter(String documentId,String CenterName,String CenterCode,Adress adress, 
    int capacity, ArrayList<String> coursesOffered, String contactEmail, String contactPhone){
        this.documentId=documentId;
        this.centerName=CenterName;
        this.centerCode=CenterCode;
        this.detailedAddress=adress;
        this.studentCapacity=capacity;
        this.coursesOffered = coursesOffered;
        this.createdOn=Timestamp.now();
        this.contactEmail=contactEmail;
        this.contactPhone=contactPhone;
        
    }
}