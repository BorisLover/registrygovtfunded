package com.example.registryGovtFunded.configurations;

import com.example.registryGovtFunded.services.TraningCenterService;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class RestResourceMainConfig extends ResourceConfig{
    public RestResourceMainConfig(){
        System.out.println("Registring Main config");
        register(TraningCenterService.class);
        
    }
    
}
