package com.example.registryGovtFunded.configurations;

import java.io.File;
//import java.io.File;
import java.io.FileInputStream;
import java.util.Objects;

import javax.annotation.PostConstruct;

import com.example.registryGovtFunded.traningCenterApplication;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import org.springframework.stereotype.Service;
@Service
public class FirebaseInitialisation {

    @PostConstruct
    public void initialization(){
        FileInputStream serviceAccount = null;
        try{
            try{
                ClassLoader classloader = traningCenterApplication.class.getClassLoader();
                File file = new File(Objects.requireNonNull(classloader.getResource("serviceAccountKey.json")).getFile());
                serviceAccount = new FileInputStream(file.getAbsolutePath());
            }catch(Exception e){
               e.printStackTrace();
            }
            
            FirebaseOptions options = FirebaseOptions.builder()
            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
            .setDatabaseUrl("https://registrygovtfunded-default-rtdb.firebaseio.com")
            .build();

            FirebaseApp.initializeApp(options);
        } catch(Exception e){
            e.printStackTrace();
       }
    }

}
