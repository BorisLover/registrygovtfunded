package com.example.registryGovtFunded.services;


import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.example.registryGovtFunded.models.TraningCenter;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;

import org.springframework.http.ResponseEntity;




@Path("/traningCenter")
public class TraningCenterService {
    private static final String COLLECTION_NAME = "traningCenter";
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<TraningCenter> getAllTraningCenters() throws InterruptedException, ExecutionException {
      Firestore dbFirestore = FirestoreClient.getFirestore();
      ApiFuture<QuerySnapshot> futur = dbFirestore.collection(COLLECTION_NAME).get();
      List<QueryDocumentSnapshot> docs = futur.get().getDocuments();
      TraningCenter traningCenter;
      ArrayList <TraningCenter> traningCenters = new ArrayList<TraningCenter>();
      for (QueryDocumentSnapshot doc : docs){
        traningCenter=doc.toObject(TraningCenter.class);
        traningCenter.setDocumentId(doc.getId());
        traningCenters.add(traningCenter);
      }
      return traningCenters;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String createTraningString(@Valid TraningCenter traningCenter) throws InterruptedException, ExecutionException 
    {
      Firestore dbFirestore = FirestoreClient.getFirestore();
      ApiFuture<DocumentReference> addedDocRef = dbFirestore.collection(COLLECTION_NAME).add(traningCenter);
      TraningCenter traningCentr = addedDocRef.get().get().get().toObject(TraningCenter.class);
      traningCentr.setDocumentId(addedDocRef.get().getId());
      return traningCentr.toString();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public String updateTraningString(@PathParam("id") String id, @Valid TraningCenter traningCenter) throws InterruptedException, ExecutionException 
    {
      Firestore dbFirestore = FirestoreClient.getFirestore();
      ApiFuture<WriteResult> addedDocRef = dbFirestore.collection(COLLECTION_NAME).document(id).set(traningCenter);
      return "Update time : " + addedDocRef.get().getUpdateTime();
    }


    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public TraningCenter getTraningCentersById(@PathParam("id") String id) throws URISyntaxException, InterruptedException, ExecutionException 
    {
      Firestore dbFirestore = FirestoreClient.getFirestore();
      DocumentReference ducRef = dbFirestore.collection(COLLECTION_NAME).document(id);
      ApiFuture<DocumentSnapshot> futur = ducRef.get();
      DocumentSnapshot duc=futur.get();
      TraningCenter traningCenter;
      if(duc.exists()) {
        traningCenter = duc.toObject(TraningCenter.class);
        traningCenter.setDocumentId(duc.getId());
        return traningCenter;
      }
      return null;
    }
   
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public String deleteTraningCenters(@PathParam("id") String id) throws URISyntaxException, InterruptedException, ExecutionException {
      Firestore dbFirestore = FirestoreClient.getFirestore();
      ApiFuture<WriteResult> writeRes = dbFirestore.collection(COLLECTION_NAME).document(id).delete();
      return "Successfully deleted  "+ writeRes.get().getUpdateTime().toString();
    }
    @GET
    @Path("/test")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<String> testGetEndPoints(){
      return ResponseEntity.ok("Test is working");
    } 

    
}
